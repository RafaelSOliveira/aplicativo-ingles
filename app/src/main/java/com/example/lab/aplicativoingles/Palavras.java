package com.example.lab.aplicativoingles;

import java.util.ArrayList;
import java.util.Collections;

public class Palavras {

    private ArrayList<Palavra> palavras ;

    public Palavras(){

        palavras = new ArrayList<>();

        Palavra p1 = new Palavra("Apple", "Maçã", R.mipmap.maca);
        Palavra p2 = new Palavra("Apartment", "Apartamento", R.mipmap.apartamento);
        Palavra p3 = new Palavra("Bicycle", "Bicicleta", R.mipmap.bicicleta);
        Palavra p4 = new Palavra("Airplane", "Avião", R.mipmap.aviao);
        Palavra p5 = new Palavra("Ball", "Bola", R.mipmap.bola);
        Palavra p6 = new Palavra("Coffe", "Café", R.mipmap.cafe);
        Palavra p7 = new Palavra("Truck", "Caminhão", R.mipmap.caminhao);
        Palavra p8 = new Palavra("Helmet", "Capacete", R.mipmap.capacete);
        Palavra p9 = new Palavra("Car", "Carro", R.mipmap.carro);
        Palavra p10 = new Palavra("Home", "Casa", R.mipmap.casa);
        Palavra p11 = new Palavra("Cellphone", "Celular", R.mipmap.celular);
        Palavra p12 = new Palavra("Key", "Chave", R.mipmap.chave);
        Palavra p13 = new Palavra("Rain", "Chuva", R.mipmap.chuva);
        Palavra p14 = new Palavra("Heart", "Coração", R.mipmap.coracao);
        Palavra p15 = new Palavra("Dice", "Dado", R.mipmap.dado);
        Palavra p16 = new Palavra("Star", "Estrela", R.mipmap.estrela);
        Palavra p17 = new Palavra("Rocket", "Foguete", R.mipmap.foguete);
        Palavra p18 = new Palavra("Magnifying Glass", "Lupa", R.mipmap.lupa);
        Palavra p19 = new Palavra("Suitcase", "Mala", R.mipmap.mala);
        Palavra p20 = new Palavra("Watermelon", "Melancia", R.mipmap.melancia);
        Palavra p21 = new Palavra("Motorcycle", "Moto", R.mipmap.moto);
        Palavra p22 = new Palavra("Glasses", "Óculos", R.mipmap.oculos);
        Palavra p23 = new Palavra("Eggs", "Ovos", R.mipmap.ovo);
        Palavra p24 = new Palavra("Bread", "Pão", R.mipmap.pao);
        Palavra p25 = new Palavra("Door", "Porta", R.mipmap.porta);
        Palavra p26 = new Palavra("Gift", "Presente", R.mipmap.presente);
        Palavra p27 = new Palavra("Sun", "Sol", R.mipmap.sol);
        Palavra p28 = new Palavra("Telephone", "Telefone", R.mipmap.telefone);
        Palavra p29 = new Palavra("Tomato", "Tomate", R.mipmap.tomate);
        Palavra p30 = new Palavra("Grape", "Uva", R.mipmap.uva);

        palavras.add(p1);
        palavras.add(p2);
        palavras.add(p3);
        palavras.add(p4);
        palavras.add(p5);
        palavras.add(p6);
        palavras.add(p7);
        palavras.add(p8);
        palavras.add(p9);
        palavras.add(p10);
        palavras.add(p11);
        palavras.add(p12);
        palavras.add(p13);
        palavras.add(p14);
        palavras.add(p15);
        palavras.add(p16);
        palavras.add(p17);
        palavras.add(p18);
        palavras.add(p19);
        palavras.add(p20);
        palavras.add(p21);
        palavras.add(p22);
        palavras.add(p23);
        palavras.add(p24);
        palavras.add(p25);
        palavras.add(p26);
        palavras.add(p27);
        palavras.add(p28);
        palavras.add(p29);
        palavras.add(p30);


    }


    public ArrayList<Palavra> getPalavras() {
        return palavras;
    }

    public void setPalavras(ArrayList<Palavra> palavras) {
        this.palavras = palavras;
    }

    public void embaralhar(){
        Collections.shuffle(this.palavras);
    }

}
