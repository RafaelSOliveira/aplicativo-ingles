package com.example.lab.aplicativoingles;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Tela01 extends Activity implements View.OnClickListener {

    private ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela01);

        logo = (ImageView)findViewById(R.id.imageView);
        logo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view == logo){
            Intent i = new Intent(this, Tela02.class);
              startActivity(i);

        }

    }

}
