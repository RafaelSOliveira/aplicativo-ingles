package com.example.lab.aplicativoingles;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Tela05 extends Activity implements View.OnClickListener {
    private Button btnReiniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela05);
       btnReiniciar = (Button)findViewById(R.id.btnReiniciar);
       btnReiniciar.setOnClickListener(this);
    }
    public void onClick(View view) {
        if(view == btnReiniciar){
            Intent i = new Intent(this, Tela01.class);
            startActivity(i);
        }
    }
}

