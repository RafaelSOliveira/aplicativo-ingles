package com.example.lab.aplicativoingles;

public class Frase {

    private String ingles;
    private String portuguese;
    private String ingres;

    public Frase(){ }

    public Frase(String ingles, String portuguese, String ingres){
        this.ingles = ingles;
        this.portuguese = portuguese;
        this.ingres = ingres;
    }

    public String getIngles() {
        return ingles;
    }

    public void setIngles(String ingles) {
        this.ingles = ingles;
    }

    public String getPortuguese() {
        return portuguese;
    }

    public void setPortuguese(String portuguese) {
        this.portuguese = portuguese;
    }

    public String getIngres() {
        return ingres;
    }

    public void setIngres(String ingres) {
        this.ingres = ingres;
    }
}
