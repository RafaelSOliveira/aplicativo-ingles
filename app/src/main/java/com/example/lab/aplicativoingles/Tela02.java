package com.example.lab.aplicativoingles;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Random;

public class Tela02 extends Activity implements View.OnClickListener{

    private ImageView imagem;
    private TextView txtIngles;
    private TextView txtPortugues;

    private LinearLayout panelTela02;

    private Palavra palavra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela02);

        imagem = findViewById(R.id.imagemTela02);
        txtIngles = findViewById(R.id.txtInglesTela02);
        txtPortugues = findViewById(R.id.txtPortuguesTela02);

        panelTela02 = findViewById(R.id.panelTela02);
        panelTela02.setOnClickListener(this);

        int min = 0;
        int max = 0;

        Palavras palavras = new Palavras();
        max = palavras.getPalavras().size();

        Random rand = new Random();
        int valor = rand.nextInt(max - min) + min;

        palavra = palavras.getPalavras().get(valor);

        txtIngles.setText(palavra.getIngles());
        txtPortugues.setText(palavra.getPortugues());
        imagem.setImageResource(palavra.getImagem());

    }

    @Override
    public void onClick(View view) {
        if(view == panelTela02){
            Intent i = new Intent(this, Tela03.class);
            Bundle b = new Bundle();
            b.putString("ingles", palavra.getIngles());
            b.putString("portugues", palavra.getPortugues());
            i.putExtras(b);
            startActivity(i);
        }
    }
}
