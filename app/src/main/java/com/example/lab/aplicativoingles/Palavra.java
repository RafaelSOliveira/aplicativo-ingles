package com.example.lab.aplicativoingles;

public class Palavra {

    private String ingles;
    private String portugues;
    private int imagem;

    public Palavra(){ }

    public Palavra(String ingles, String portugues, int imagem){
        this.ingles = ingles;
        this.portugues = portugues;
        this.imagem = imagem;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    public String getPortugues() {
        return portugues;
    }

    public void setPortugues(String portugues) {
        this.portugues = portugues;
    }

    public String getIngles() {
        return ingles;
    }

    public void setIngles(String ingles) {
        this.ingles = ingles;
    }
}
