package com.example.lab.aplicativoingles;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Tela04 extends Activity implements View.OnClickListener {

    private TextView txtIngles2;
    private TextView txtPortugues2;
    private Button btn1Tela04, btn2Tela04, btnVoltar;
    private int certo;
    private ArrayList<Palavra> palavras;
    private ArrayList<Frase> frases;
    private String ingles,portugues, portuguese,ingres;
    private Frase frase;
    private Palavra palavra;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela04);

        btn1Tela04 = (Button)findViewById(R.id.btn1Tela04);
        btn1Tela04.setOnClickListener(this);
        btn2Tela04 = (Button)findViewById(R.id.btn2Tela04);
        btn2Tela04.setOnClickListener(this);
        btnVoltar = (Button)findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(this);

        Intent i = getIntent();
        if(i != null){
            Bundle b = i.getExtras();
            if(b != null){
                ingles = b.getString("ingles");
            }
        }

        txtIngles2 = findViewById(R.id.txtInglesTela04);
        txtPortugues2 = findViewById(R.id.txtPortuguesTela04);

        Frases frases = new Frases();

        int valor = 0;

        for(int j = 0; j < frases.getFrases().size(); j++){
            if(frases.getFrases().get(j).getIngles().equals(ingles)) {
                valor = j;
                break;
            }
        }

        frase = frases.getFrases().get(valor);

        txtIngles2.setText(frase.getIngres());
        txtPortugues2.setText(frase.getPortuguese());

        iniciarJogo();

    }
    public void iniciarJogo() {

        Palavras p = new Palavras();
        Palavra palavra1 = new Palavra();

        do{
            Collections.shuffle(p.getPalavras());
        }while(p.getPalavras().get(0).getIngles().equals(ingles));

        palavra1 = p.getPalavras().get(0);

        Random r = new Random();
        int posicao = r.nextInt(2);
        if (posicao == 0) {
            btn2Tela04.setText(palavra1.getIngles());

            btn1Tela04.setText(ingles);

            certo = 1;
        }else {
            btn1Tela04.setText(palavra1.getIngles());

            btn2Tela04.setText(ingles);
            certo = 2;
        }



    }


    @Override
    public void onClick (View view){

        if (view == btn1Tela04) {
            if (certo == 1) {
                btn1Tela04.setBackgroundColor(Color.GREEN);
            } else {
                btn1Tela04.setVisibility(View.INVISIBLE);
            }
        }
        if (view == btn2Tela04) {
            if (certo == 2) {
                btn2Tela04.setBackgroundColor(Color.GREEN);
            } else {
                btn2Tela04.setVisibility(View.INVISIBLE);
            }
        }
        if(view == btnVoltar){
            Intent i = new Intent(this, Tela05.class);
            startActivity(i);
        }
    }}


