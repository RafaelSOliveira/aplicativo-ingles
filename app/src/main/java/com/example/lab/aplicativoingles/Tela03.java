package com.example.lab.aplicativoingles;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Tela03 extends Activity implements View.OnClickListener {

    private TextView txtIngles;
    private Button btn1Tela03, btn2Tela03, btn3Tela03, btnProx;
    private int certo;
    private ArrayList<Palavra> palavras;
    private String ingles, portugues;
    private Frase frase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela03);


        btn1Tela03 = (Button) findViewById(R.id.btn1Tela03);
        btn1Tela03.setOnClickListener(this);
        btn2Tela03 = (Button) findViewById(R.id.btn2Tela03);
        btn2Tela03.setOnClickListener(this);
        btn3Tela03 = (Button) findViewById(R.id.btn3Tela03);
        btn3Tela03.setOnClickListener(this);
        btnProx = (Button) findViewById(R.id.btnProx);
        btnProx.setOnClickListener(this);

        Intent i = getIntent();
        if (i != null) {
            Bundle b = i.getExtras();
            if (b != null) {
                ingles = b.getString("ingles");
                portugues = b.getString("portugues");
            }
        }

        txtIngles = (TextView) findViewById(R.id.txtInglesTela03);

        txtIngles.setText(ingles);


        iniciarJogo();

    }

    public void iniciarJogo() {

        Palavras p = new Palavras();
        Palavra palavra1 = new Palavra();
        Palavra palavra2 = new Palavra();


        do {
            Collections.shuffle(p.getPalavras());
        }
        while (p.getPalavras().get(0).getPortugues().equals(portugues) || p.getPalavras().get(1).getPortugues().equals(portugues));

        palavra1 = p.getPalavras().get(0);
        palavra2 = p.getPalavras().get(1);

        txtIngles.setText(ingles);
        Random r = new Random();
        int posicao = r.nextInt(3);
        if (posicao == 0) {
            btn2Tela03.setText(palavra1.getPortugues());
            btn3Tela03.setText(palavra2.getPortugues());

            btn1Tela03.setText(portugues);

            certo = 1;
        }
        if (posicao == 1) {
            btn1Tela03.setText(palavra1.getPortugues());
            btn3Tela03.setText(palavra2.getPortugues());

            btn2Tela03.setText(portugues);
            certo = 2;
        }
        if (posicao == 2) {
            btn1Tela03.setText(palavra1.getPortugues());
            btn2Tela03.setText(palavra2.getPortugues());

            btn3Tela03.setText(portugues);
            certo = 3;
        }


    }


    @Override
    public void onClick(View view) {

        if (view == btn1Tela03) {
            if (certo == 1) {
                btn1Tela03.setBackgroundColor(Color.GREEN);
            } else {
                btn1Tela03.setVisibility(View.INVISIBLE);
            }
        }
        if (view == btn2Tela03) {
            if (certo == 2) {
                btn2Tela03.setBackgroundColor(Color.GREEN);
            } else {
                btn2Tela03.setVisibility(View.INVISIBLE);
            }
        }
        if (view == btn3Tela03) {
            if (certo == 3) {
                btn3Tela03.setBackgroundColor(Color.GREEN);
            } else {
                btn3Tela03.setVisibility(View.INVISIBLE);
            }
        }

       if(view == btnProx){
            Intent i = new Intent(this, Tela04.class);
           Bundle b = new Bundle();
           b.putString("ingles", ingles);
           i.putExtras(b);
            startActivity(i);
        }

    }}




