package com.example.lab.aplicativoingles;

import java.util.ArrayList;
import java.util.Collections;



public class Frases {

    private static ArrayList<Frase> frases;

    public Frases(){

         frases = new ArrayList<>();

        Frase f1 = new Frase("Apple", "Esta é uma maçã.", "This is an _____.");
        Frase f2 = new Frase("Apartment","Para viver em um apartamento você não pode fazer barulhos.","To live in an _____ you can't make noises." );
        Frase f3 = new Frase("Bicycle","Eu gosto de andar de bicicleta.", "I like to ride a _____.");
        Frase f4 = new Frase("Airplane","Eu não gosto de viajar em aviões.", "I don't like to travel on _____.");
        Frase f5 = new Frase("Ball","Eu gosto de chutar bola.", "I like to kick a _____.");
        Frase f6 = new Frase("Coffe","Café é bom para manter você acordado.","_____ is good to keep you awake.");
        Frase f7 = new Frase("Truck","Caminhões carregam coisas.","_____ carry things.");
        Frase f8 = new Frase("Helmet","O capacete protege sua cabeça.","The _____ protects your head.");
        Frase f9 = new Frase("Car","O carro está na estrada.","The ____ is on the road.");
        Frase f10 = new Frase("Home","Eu moro em uma casa.","I live in a ____.");
        Frase f11 = new Frase("Cellphone","Todo mundo tem um celular.","Everybody has a _____.");
        Frase f12 = new Frase("Key","Eu tenho a chave da porta da minha casa.","I have the ___ for the door of my house.");
        Frase f13 = new Frase("Rain","A chuva limpa as ruas.","The ____ clears the street.");
        Frase f14 = new Frase("Heart","Este coração é vermelho.","This ____ is red.");
        Frase f15 = new Frase("Dice","Eu joguei os dados.","I played the ____.");
        Frase f16 = new Frase("Star","Estrelas só aparecem à noite.","____ only appear at night.");
        Frase f17 = new Frase("Rocket","O foguete pode ir ao espaço.","The _____ can go to space.");
        Frase f18 = new Frase("Magnifying Glass","A lupa ajuda a enxergar coisas pequenas.","The _________ helps to see small things.");
        Frase f19 = new Frase("Suitcase","Eu uso minha mala para viajar.","I use my _____ to travel.");
        Frase f20 = new Frase("Watermelon","Melancias tem muita água.","______ have a lot of water.");
        Frase f21 = new Frase("Motorcycle","Para dirigir uma motocicleta você precisa de um capacete.","To drive a _____ you need a helmet.");
        Frase f22 = new Frase("Glasses","Eu tenho que usar óculos.","I have to use _____.");
        Frase f23 = new Frase("Eggs","A galinha põe ovos.","The chicken lays ___.");
        Frase f24 = new Frase("Bread","Eu gosto de pão no café da manhã.","I like ____ at breakfast.");
        Frase f25 = new Frase("Door","A porta está fechada.","The ___ is closed.");
        Frase f26 = new Frase("Gift","Eu amo ganhar presentes.","I love earn ____.");
        Frase f27 = new Frase("Sun","O sol ilumina o dia.","The ___ illuminates the day.");
        Frase f28 = new Frase("Telephone","Eu uso o telefone para fazer ligações.","I use the ______ to make calls.");
        Frase f29 = new Frase("Tomato","Tomate é um fruto.","_____ is a fruit.");
        Frase f30 = new Frase("Grape","Uvas sãos roxas.","_____ are purple.");

        frases.add(f1);
        frases.add(f2);
        frases.add(f3);
        frases.add(f4);
        frases.add(f5);
        frases.add(f6);
        frases.add(f7);
        frases.add(f8);
        frases.add(f9);
        frases.add(f10);
        frases.add(f11);
        frases.add(f12);
        frases.add(f13);
        frases.add(f14);
        frases.add(f15);
        frases.add(f16);
        frases.add(f17);
        frases.add(f18);
        frases.add(f19);
        frases.add(f20);
        frases.add(f21);
        frases.add(f22);
        frases.add(f23);
        frases.add(f24);
        frases.add(f25);
        frases.add(f26);
        frases.add(f27);
        frases.add(f28);
        frases.add(f29);
        frases.add(f30);

    }
    public static ArrayList<Frase> getFrases() { return frases ; }

    public void setFrases(ArrayList<Frase> frases) {
        this.frases = frases;
    }

}
